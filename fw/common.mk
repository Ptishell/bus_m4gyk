CROSS=arm-none-eabi
STDEVICE=STM32F072xB
STFLASH ?= st-flash

CC=$(CROSS)-gcc
AS=$(CROSS)-as
CFLAGS +=-Wall -g -std=c99 -Os -MMD\
	 -mlittle-endian -mcpu=cortex-m0  -march=armv6-m -mthumb \
	 -ffunction-sections -fdata-sections
CPPFLAGS += -D$(STDEVICE) \
	    -I./inc
LDFLAGS += -Tlds/$(LD_SCRIPT) -Wl,--gc-sections

SIZE=$(CROSS)-size
OBJCOPY=$(CROSS)-objcopy
OBJDUMP=$(CROSS)-objdump
GDB=$(CROSS)-gdb

TARGET	?= MISSING_TARGET
ELF	= $(TARGET).elf
HEX	= $(TARGET).hex
BIN	= $(TARGET).bin
LST	= $(TARGET).lst

SRC := $(addprefix ./src/, $(SRC))

SRC += $(SRC_VENDOR)

ASM := $(addprefix ./src/, $(ASM))
OBJ = $(SRC:.c=.o) $(ASM:.s=.o)
DEPS = $(SRC:.c=.d)

all:: $(BIN) $(LST)

$(LST): $(ELF)
	$(OBJDUMP) -DSt $< > $@

$(BIN): $(ELF)
	$(OBJCOPY) -O binary $< $@

$(ELF): $(OBJ)
	$(CC) $(CFLAGS) $(CPPFLAGS) $(LDFLAGS) $(OBJ) -o $@
	$(SIZE) --radix=16 --format=sysv $@

flash:: $(BIN)
	$(STFLASH) --reset write $(BIN) $(FLASH_ADDR)

debug:: $(ELF)
	$(GDB) $< -ex 'target remote :4242'

clean::
	$(RM) $(ELF) $(HEX) $(OBJ) $(DEPS)

distclean:: clean
	$(RM) config.mk

-include $(DEPS)
