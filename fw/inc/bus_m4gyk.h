#ifndef BUS_M4GYK_H
#define BUS_M4GYK_H

#include "stm32f0xx_hal.h"

typedef enum 
{
  LED1 = 0,
  /* Color led aliases */
  LED_RED    = LED1,
}Led_TypeDef;


#define LEDn                        1

#define LED1_PIN                    GPIO_PIN_0
#define LED1_GPIO_PORT              GPIOA
#define LED1_GPIO_CLK_ENABLE()      __GPIOA_CLK_ENABLE()
#define LED1_GPIO_CLK_DISABLE()     __GPIOA_CLK_DISABLE()
#define LEDx_GPIO_CLK_ENABLE(__LED__) \
     (((__LED__) == LED1) ? LED1_GPIO_CLK_ENABLE() : 0 )

uint32_t BSP_GetVersion(void);
void BSP_LED_Init(Led_TypeDef Led);
void BSP_LED_On(Led_TypeDef Led);
void BSP_LED_Off(Led_TypeDef Led);
void BSP_LED_Toggle(Led_TypeDef Led);

#endif /* BUS_M4GYK_H */
