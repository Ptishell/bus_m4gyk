#include "bus_m4gyk.h"

#define __BM_BSP_VERSION_MAIN   (0x00)
#define __BM_BSP_VERSION_SUB1   (0x00)
#define __BM_BSP_VERSION_SUB2   (0x00)
#define __BM_BSP_VERSION_RC     (0x00)

#define __BM_BSP_VERSION         \
    ((__BM_BSP_VERSION_MAIN << 24) \
     | (__BM_BSP_VERSION_SUB1 << 16) \
     | (__BM_BSP_VERSION_SUB2 << 8 ) \
     | (__BM_BSP_VERSION_RC))

GPIO_TypeDef* LED_PORT[LEDn] = {LED1_GPIO_PORT};

const uint16_t LED_PIN[LEDn] = {LED1_PIN};

uint32_t BSP_GetVersion(void)
{
    return __BM_BSP_VERSION;
}

void BSP_LED_Init(Led_TypeDef Led)
{
    GPIO_InitTypeDef  GPIO_InitStruct;

    /* Enable the GPIO_LED Clock */
    LEDx_GPIO_CLK_ENABLE(Led);

    /* Configure the GPIO_LED pin */
    GPIO_InitStruct.Pin = LED_PIN[Led];
    GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
    GPIO_InitStruct.Pull = GPIO_PULLUP; /* GPIO_PuPd_DOWN */
    GPIO_InitStruct.Speed = GPIO_SPEED_HIGH;

    HAL_GPIO_Init(LED_PORT[Led], &GPIO_InitStruct);

    HAL_GPIO_WritePin(LED_PORT[Led], LED_PIN[Led], GPIO_PIN_RESET); 
}

void BSP_LED_On(Led_TypeDef Led)
{
    HAL_GPIO_WritePin(LED_PORT[Led], LED_PIN[Led], GPIO_PIN_SET); 
}

void BSP_LED_Off(Led_TypeDef Led)
{
    HAL_GPIO_WritePin(LED_PORT[Led], LED_PIN[Led], GPIO_PIN_RESET); 
}

void BSP_LED_Toggle(Led_TypeDef Led)
{
    HAL_GPIO_TogglePin(LED_PORT[Led], LED_PIN[Led]);
}
