#!/usr/bin/env python2

import footgen

f = footgen.Footgen("US8", output_format="geda")
f.pins = 8
f.pitch = 0.5
f.width = 2.1
f.padheight = 0.25
f.padwidth = 1
f.so()
f.silk_crop(2.5, 3, pin1="circle")
f.finish()
