# Pin name action command file

# Start of element D1
ChangePinName(D1, 2, 2)
ChangePinName(D1, 1, 1)

# Start of element R9
ChangePinName(R9, 1, 1)
ChangePinName(R9, 2, 2)

# Start of element U5
ChangePinName(U5, 3, VIN)
ChangePinName(U5, 2, VOUT)
ChangePinName(U5, 1, GND)

# Start of element C6
ChangePinName(C6, 2, 2)
ChangePinName(C6, 1, 1)

# Start of element C5
ChangePinName(C5, 2, 2)
ChangePinName(C5, 1, 1)

# Start of element USB
ChangePinName(USB, 5, GND)
ChangePinName(USB, 4, ID)
ChangePinName(USB, 3, D+)
ChangePinName(USB, 2, D-)
ChangePinName(USB, 1, VCC)

# Start of element R8
ChangePinName(R8, 1, 1)
ChangePinName(R8, 2, 2)

# Start of element R7
ChangePinName(R7, 1, 1)
ChangePinName(R7, 2, 2)

# Start of element BOOT
ChangePinName(BOOT, 1, 1)
ChangePinName(BOOT, 2, 2)

# Start of element D2
ChangePinName(D2, 2, 2)
ChangePinName(D2, 1, 1)

# Start of element R6
ChangePinName(R6, 1, 1)
ChangePinName(R6, 2, 2)

# Start of element C4
ChangePinName(C4, 2, 2)
ChangePinName(C4, 1, 1)

# Start of element C3
ChangePinName(C3, 2, 2)
ChangePinName(C3, 1, 1)

# Start of element U4
ChangePinName(U4, 2, 2)
ChangePinName(U4, 1, 1)

# Start of element I2C/CAN
ChangePinName(I2C/CAN, 5, 5)
ChangePinName(I2C/CAN, 3, 3)
ChangePinName(I2C/CAN, 1, 1)
ChangePinName(I2C/CAN, 6, 6)
ChangePinName(I2C/CAN, 4, 4)
ChangePinName(I2C/CAN, 2, 2)

# Start of element R5
ChangePinName(R5, 1, 1)
ChangePinName(R5, 2, 2)

# Start of element R4
ChangePinName(R4, 1, 1)
ChangePinName(R4, 2, 2)

# Start of element R3
ChangePinName(R3, 1, 1)
ChangePinName(R3, 2, 2)

# Start of element U3
ChangePinName(U3, 8, EN)
ChangePinName(U3, 7, VREF2)
ChangePinName(U3, 6, SCL2)
ChangePinName(U3, 5, SDA2)
ChangePinName(U3, 4, SDA1)
ChangePinName(U3, 3, SCL1)
ChangePinName(U3, 2, VREF1)
ChangePinName(U3, 1, GND)

# Start of element R2
ChangePinName(R2, 1, 1)
ChangePinName(R2, 2, 2)

# Start of element R1
ChangePinName(R1, 1, 1)
ChangePinName(R1, 2, 2)

# Start of element C2
ChangePinName(C2, 2, 2)
ChangePinName(C2, 1, 1)

# Start of element C1
ChangePinName(C1, 2, 2)
ChangePinName(C1, 1, 1)

# Start of element POWER
ChangePinName(POWER, 1, 1)
ChangePinName(POWER, 4, 4)
ChangePinName(POWER, 2, 2)
ChangePinName(POWER, 3, 3)

# Start of element UART/SPI
ChangePinName(UART/SPI, 5, 5)
ChangePinName(UART/SPI, 3, 3)
ChangePinName(UART/SPI, 1, 1)
ChangePinName(UART/SPI, 6, 6)
ChangePinName(UART/SPI, 4, 4)
ChangePinName(UART/SPI, 2, 2)

# Start of element U2
ChangePinName(U2, 7, CANH)
ChangePinName(U2, 6, CANL)
ChangePinName(U2, 4, R)
ChangePinName(U2, 3, Vcc)
ChangePinName(U2, 2, GND)
ChangePinName(U2, 1, D)

# Start of element SWD
ChangePinName(SWD, 1, 1)
ChangePinName(SWD, 4, 4)
ChangePinName(SWD, 2, 2)
ChangePinName(SWD, 3, 3)

# Start of element U1
ChangePinName(U1, 48, VDD)
ChangePinName(U1, 47, VSS)
ChangePinName(U1, 46, PB9)
ChangePinName(U1, 45, PB8)
ChangePinName(U1, 44, BOOT)
ChangePinName(U1, 43, PB7)
ChangePinName(U1, 42, PB6)
ChangePinName(U1, 41, PB5)
ChangePinName(U1, 40, PB4)
ChangePinName(U1, 39, PB3)
ChangePinName(U1, 38, PA15)
ChangePinName(U1, 37, PA14)
ChangePinName(U1, 36, VDDIO2)
ChangePinName(U1, 35, VSS)
ChangePinName(U1, 34, PA13)
ChangePinName(U1, 33, PA12)
ChangePinName(U1, 32, PA11)
ChangePinName(U1, 31, PA10)
ChangePinName(U1, 30, PA9)
ChangePinName(U1, 29, PA8)
ChangePinName(U1, 28, PB15)
ChangePinName(U1, 27, PB14)
ChangePinName(U1, 26, PB13)
ChangePinName(U1, 25, PB12)
ChangePinName(U1, 24, VDD)
ChangePinName(U1, 23, VSS)
ChangePinName(U1, 22, PB11)
ChangePinName(U1, 21, PB10)
ChangePinName(U1, 20, PB2)
ChangePinName(U1, 19, PB1)
ChangePinName(U1, 18, PB0)
ChangePinName(U1, 17, PA7)
ChangePinName(U1, 16, PA6)
ChangePinName(U1, 15, PA5)
ChangePinName(U1, 14, PA4)
ChangePinName(U1, 13, PA3)
ChangePinName(U1, 12, PA2)
ChangePinName(U1, 11, PA1)
ChangePinName(U1, 10, PA0)
ChangePinName(U1, 9, VDDA)
ChangePinName(U1, 8, VSSA)
ChangePinName(U1, 7, RST)
ChangePinName(U1, 6, PF1/OSC_OUT)
ChangePinName(U1, 5, PF0/OSC_IN)
ChangePinName(U1, 4, PC15/OSC32_OUT)
ChangePinName(U1, 3, PC14/OSC32_IN)
ChangePinName(U1, 2, PC13)
ChangePinName(U1, 1, VBAT)
